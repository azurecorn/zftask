<?php

class Application_Form_Subscribe extends Zend_Form
{

    public function init()
    {
        $this->setName('subscribe');

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('EmailAddress');
        $level = new Zend_Form_Element_Text('level');
        $level->setLabel('Level')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('Float');

        $submit = new Zend_Form_Element_Submit('Submit');
        $submit->setAttrib('id', 'submitbutton');

        $this->addElements(array( $email, $level, $submit));
    }

}

