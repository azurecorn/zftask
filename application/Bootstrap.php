<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initMail()
    {
        $controller = Zend_Controller_Front::getInstance();

        $reg_config = new Zend_Config($this->getOptions());
        Zend_Registry::set('reg_config', $reg_config);

        try {
            $config = array(
                'auth' => $reg_config->mail->auth,
                'username' => $reg_config->mail->username,
                'password' => $reg_config->mail->password,
                'ssl' => $reg_config->mail->ssl,
                'port' => $reg_config->mail->port
            );

            $mailTransport = new Zend_Mail_Transport_Smtp($reg_config->mail->host, $config);
            Zend_Mail::setDefaultTransport($mailTransport);
        } catch (Zend_Exception $e){
            //D
        }

        //register plugin
        $controller->registerPlugin(new Application_Plugin_Common());
    }

}

