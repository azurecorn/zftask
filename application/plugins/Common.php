<?php
/**
 *
 * Common application-wide methods
 *
 */
class Application_Plugin_Common extends Zend_Controller_Plugin_Abstract {


	/*
	 * Send Email using html templates
	 */
	static public function sendEmail($to)
	{

		$reg_config = Zend_Registry::get('reg_config');

		//Prepare email
		$mail = new Zend_Mail();
		$mail->addTo($to);
		$mail->setSubject($reg_config->mail->subject);
		$mail->setFrom($reg_config->mail->fromEmail, $reg_config->mail->fromName);

		// use email templates
		$html = new Zend_View();
		$html->setScriptPath(APPLICATION_PATH . '/views/emails/');
		$body = $html->render("notify.phtml");

		$mail->setBodyHtml($body);

		$sent = true;
		try {
			$mail->send();
		} catch (Exception $e){
			$sent = false;
		}

		if($sent){
			//Mail was sent successfully.
		} else {
			//Mail failed to send.
		}
	}
}
