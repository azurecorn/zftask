<?php

class Application_Model_DbTable_Subscribers extends Zend_Db_Table_Abstract
{

    protected $_name = 'subscribers';

    public function email_exists($email)
    {
        $subscribers = $this->fetchAll();
        $result = false;

        foreach($subscribers as $subscriber){
            if($subscriber->email == $email){
                $result = true;
            }
        }
        return $result;
    }

    public function subscribe($email, $level)
    {
        $data = array(
            'email' => $email,
            'level' => $level,
        );
        $this->insert($data);
    }

    public function getUsersToNotify($bitcoin_value)
    {

        $subscribers = $this->fetchAll();
        $to_noify = [];

        foreach($subscribers as $subscriber){
            if($bitcoin_value >= $subscriber->level){
                $to_noify[] = $subscriber->email;
            }
        }
        return $to_noify;
    }


}

