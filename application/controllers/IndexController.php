<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $ratesModel = new Application_Model_DbTable_BitcoinConversionRate();
        //limit 20
        $all_rates = $ratesModel->getAll(20);

        //process form
        $form = new Application_Form_Subscribe();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $email = $form->getValue('email');
                $level = $form->getValue('level');
                $subscribersModel = new Application_Model_DbTable_Subscribers();
                if(!$subscribersModel->email_exists($email)){
                    $subscribersModel->subscribe($email, $level);
                }
                $this->_helper->redirector('index');
            }else {
                $form->populate($formData);
            }
        }

        //prepare data for chart
        $rates_formatted_for_chart = [];
        foreach($all_rates as $key => $rate){
            $rates_formatted_for_chart['time'][] = $rate->time;
            $rates_formatted_for_chart['rates'][] = $rate->bitcoin_eur;

        }

        //view
        $this->view->form = $form;
        $this->view->ratesForChart = $rates_formatted_for_chart;
    }

    public function checkLevelAndSubscribeAction()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.bitcoinaverage.com/ticker/EUR/last',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true
        ));

        $result = curl_exec($curl);
        curl_close ($curl);

        if(is_numeric($result)){
            $ratesModel = new Application_Model_DbTable_BitcoinConversionRate();
            $ratesModel->add($result);
            echo $result. " rate added";
        }

        $subscribers = new Application_Model_DbTable_Subscribers();
        $emails_notify = $subscribers->getUsersToNotify($result);

        foreach($emails_notify as $email){
            Application_Plugin_Common::sendEmail($email);
        }

        die;

    }


}

