<?php

class Application_Model_DbTable_BitcoinConversionRate extends Zend_Db_Table_Abstract
{

    protected $_name = 'bitcoin_conversion_rate';

    public function add($rate)
    {
        $data = array(
            'bitcoin_eur' => $rate,
            'time' => date('Y-m-d H:i:s')
        );

        $this->insert($data);
    }

    public function getAll($limit = 20)
    {
        $all = $this->fetchAll()->toArray();
        $count = count($all);

        $select = $this->select();
        if($count && $limit < $count){
            $select->order('id ASC')->limit($limit, $count-$limit);
        }

        return $this->fetchAll($select);
    }

}

